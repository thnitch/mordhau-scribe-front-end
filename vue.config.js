// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin
const path = require('path')
const mode = process.env.VUE_APP_SCRIBE_ENV
const DEV_MODES = ['development', 'local']

const sharedPlugins = []
const devPlugins = [...sharedPlugins]
const prodPlugins = [...sharedPlugins]

module.exports = {
  configureWebpack: {
    devtool: 'source-map',

    plugins: DEV_MODES.includes(mode) ? devPlugins : prodPlugins,
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: DEV_MODES.includes(mode)
        ? '[name].[fullhash].js'
        : '[name].[chunkhash].js',
    },
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
      },
      moduleIds: DEV_MODES.includes(mode) ? 'named' : 'deterministic',
    },
    mode: DEV_MODES.includes(mode) ? 'development' : 'production',
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/scss/variables.scss";$rfs-base-value: 0.8rem !default;@import '~rfs/scss';`,
      },
    },
  },
  pwa: {
    name: process.env.VUE_APP_TITLE,
    themeColor: '#0c0e0f',
    msTileColor: '#0c0e0f',
    manifestOptions: {
      background_color: '#0c0e0f',
      display: 'standalone',
    },
    iconPaths: {
      appleTouchIcon: 'img/icons/apple-touch-icon-180x180.png',
      msTileImage: 'img/icons/mstile-150x150.png',
    },
    assetsVersion: '1',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: './src/sw.js',
      swDest: 'precache-manifest-service-worker.js',
    },
  },
  productionSourceMap: mode === 'local',
  transpileDependencies: ['vue-meta'],
}
