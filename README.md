# mordhau-scribe-front-end

Vue 3 frontend for https://mordhau-scribe.com

See [mordhau-scribe](https://gitlab.com/thnitch/mordhau-scribe) for the backend code.
