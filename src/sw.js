import { precacheAndRoute } from 'workbox-precaching'
import { clientsClaim } from 'workbox-core'

// Listens for user confirmation to update the app.
self.addEventListener('message', (e) => {
  if (!e.data) {
    return
  }

  switch (e.data) {
    case 'skipWaiting':
      self.skipWaiting()
      break
    default:
      // NOOP
      break
  }
})

clientsClaim() // Vue CLI 4 and Workbox v44

// The precaching code provided by Workbox.
precacheAndRoute(self.__WB_MANIFEST)
