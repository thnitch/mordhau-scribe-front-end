const settings = {
  apiProtocol: 'http://',
  apiHost: 'localhost:8080',
  siteProtocol: 'http://',
  siteHost: 'localhost:80',
  isProd: false,
  isStaging: false,
  trackingId: 'UA-165363454-1',
  appUpdateCheckInterval: 1000,
}
module.exports = settings
