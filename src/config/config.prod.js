const settings = {
  apiProtocol: 'https://',
  apiHost: 'api.mordhau-scribe.com:8443',
  siteProtocol: 'https://',
  siteHost: 'mordhau-scribe.com',
  isProd: true,
  isStaging: false,
  trackingId: 'UA-165363454-1',
  appUpdateCheckInterval: 30 * 60 * 1000,
}
module.exports = settings
