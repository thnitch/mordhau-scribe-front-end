import config from '@/config/config'
import { handleResponse, handleFetchError } from '@/_helpers/error-handling.js'
import { QUEUE_TYPE_PATH } from '@/_helpers/enums'

export async function fetchHistoricalTopX(queueType) {
  const requestOptions = {
    method: 'GET',
  }
  return fetch(
    `${config.endpoints.historicalTopX}${QUEUE_TYPE_PATH[queueType]}HistoricalTopX`,
    requestOptions
  ).then((response) => {
    return handleResponse(response)
  }, handleFetchError)
}
